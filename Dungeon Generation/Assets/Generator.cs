﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using UnityEditor;

/// <summary>
/// Author: Matt Gipson
/// Contact: Deadwynn@gmail.com
/// Domain: www.livingvalkyrie.net
/// 
/// Description: Generator
/// </summary>
public class Generator : MonoBehaviour {
	#region Fields

	public int minSize, maxSize;
	public int roomCount = 10;
	public float radius = 5;

	public Transform roomPrefab;
	Transform dungeon;

	#endregion
	//todo add snapping to grid
	public void Generate() {
		if (dungeon == null) {
			
			dungeon = new GameObject().transform;
		} else {
			for (int i = 0; i < dungeon.childCount; i++) {
				DestroyImmediate(dungeon.GetChild(i).gameObject);
			}
		}

		dungeon.SetParent(transform);

		for (int i = 0; i < roomCount; i++) {
			var temp = Instantiate(roomPrefab);
			temp.position = Random.insideUnitCircle * radius;
			temp.localScale = new Vector3(Random.Range(minSize, maxSize), Random.Range( minSize, maxSize ) , 1);
			temp.SetParent(dungeon);
		}
	}
}

/// <summary>
	/// Author: Matt Gipson
	/// Contact: Deadwynn@gmail.com
	/// Domain: www.livingvalkyrie.net
	/// 
	/// Description: GeneratorEditor
	/// </summary>
	[CustomEditor(typeof(Generator))]
	public class GeneratorEditor : Editor {
		public override void OnInspectorGUI() {
			Generator myTarget = (Generator) target;
			DrawDefaultInspector();

			EditorGUILayout.Space();

			if (GUILayout.Button("Generate")) {
				myTarget.Generate();
			}
		}
	}
