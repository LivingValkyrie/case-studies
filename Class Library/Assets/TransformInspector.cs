﻿using UnityEngine;
using UnityEditor;

/// <summary>
/// Author: Matt Gipson
/// Contact: Deadwynn@gmail.com
/// Domain: www.livingvalkyrie.net
/// 
/// Description: TransformInspector
/// </summary>
[CustomEditor(typeof(Transform))]
public class TransformInspector : Editor {
	#region Fields

	Vector3 old = new Vector3();
	bool xClicked = false, yClicked = false, zClicked = false, reset = false;

	#endregion

	public override void OnInspectorGUI() {
		Transform myTarget = (Transform) target;
		DrawDefaultInspector();
		EditorGUILayout.Space();
		EditorGUILayout.LabelField("----------- Start custom inspector ----------");

		#region position stuff

		GUILayout.BeginHorizontal();
		GUILayout.Label("Position");
		if (GUILayout.Button("Reset", GUILayout.Width(50), GUILayout.Height(20))) {
			//Undo.RegisterCompleteObjectUndo(myTarget, "transform reset");

			reset = !reset;

			if (reset) {
				old = myTarget.position;
				myTarget.position = Vector3.zero;
			} else {
				myTarget.position = old;
			}
		}

		if (GUILayout.Button("X", GUILayout.Width(20), GUILayout.Height(20))) {
			xClicked = !xClicked;
			Vector3 temp = myTarget.position;

			if (xClicked) {
				old.x = temp.x;
				myTarget.position = new Vector3(0, temp.y, temp.z);
			} else {
				myTarget.position = new Vector3(old.x, temp.y, temp.z);
			}
		}

		if (GUILayout.Button("Y", GUILayout.Width(20), GUILayout.Height(20))) {
			yClicked = !yClicked;
			Vector3 temp = myTarget.position;

			if (yClicked) {
				old.y = temp.y;
				myTarget.position = new Vector3(temp.x, 0, temp.z);
			} else {
				myTarget.position = new Vector3(temp.x, old.y, temp.z);
			}
		}

		if (GUILayout.Button("Z", GUILayout.Width(20), GUILayout.Height(20))) {
			zClicked = !zClicked;
			Vector3 temp = myTarget.position;

			if (zClicked) {
				old.z = temp.z;
				myTarget.position = new Vector3(temp.x, temp.y, 0);
			} else {
				myTarget.position = new Vector3(temp.x, temp.y, old.z);
			}
		}

		//todo make a new vector3 field method. need to get the buttons and fields to drop down when inspector is too small
		myTarget.localPosition = EditorGUILayout.Vector3Field("", myTarget.localPosition);

		GUILayout.EndHorizontal();

		myTarget.localEulerAngles = EditorGUILayout.Vector3Field("Rotation", myTarget.localEulerAngles);
		myTarget.localScale = EditorGUILayout.Vector3Field("Scale", myTarget.localScale);

		#endregion
	}

}

/* 
 
	if (GUILayout.Button("X", GUILayout.Width(20), GUILayout.Height(20))) {
			xClicked = !xClicked;
			Vector3 temp = myTarget.position;

			if (xClicked) {
				old.x = temp.x;
				myTarget.position = new Vector3(0, temp.y, temp.z);
			} else {
				myTarget.position = new Vector3(old.x, temp.y, temp.z);
			}
		}

		float testX = myTarget.localPosition.x;
		testX = EditorGUILayout.FloatField(testX);

		if (GUILayout.Button("Y", GUILayout.Width(20), GUILayout.Height(20))) {
			yClicked = !yClicked;
			Vector3 temp = myTarget.position;

			if (yClicked) {
				old.y = temp.y;
				myTarget.position = new Vector3(temp.x, 0, temp.z);
			} else {
				myTarget.position = new Vector3(temp.x, old.y, temp.z);
			}
		}

		float testY = myTarget.localPosition.y;
		testY = EditorGUILayout.FloatField( testY );

		if (GUILayout.Button("Z", GUILayout.Width(20), GUILayout.Height(20))) {
			zClicked = !zClicked;
			Vector3 temp = myTarget.position;

			if (zClicked) {
				old.z = temp.z;
				myTarget.position = new Vector3(temp.x, temp.y, 0);
			} else {
				myTarget.position = new Vector3(temp.x, temp.y, old.z);
			}
		}
		float testZ = myTarget.localPosition.z;
		testZ = EditorGUILayout.FloatField( testZ );

		//myTarget.localPosition = EditorGUILayout.Vector3Field("", myTarget.localPosition);

		myTarget.localPosition = new Vector3(testX, testY, testZ);
	 
	 */
