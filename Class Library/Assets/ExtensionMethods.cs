﻿using System;
using UnityEngine;

namespace LivingValkyrie {

	/// <summary>
	/// Author: Matt Gipson
	/// Contact: Deadwynn@gmail.com
	/// Domain: www.livingvalkyrie.net
	/// 
	/// Description: ExtensionMethods is a class for holding extension methods for use in unity
	/// </summary>
	public static class ExtensionMethods {

		public static float Remap(this float value, float baseMin, float baseMax, float goalMin, float goalMax) {
			return (value - baseMin) / (baseMax - baseMin) * (goalMax - goalMin) + goalMin;
		}

		public static bool ContainsParam(this Animator _Anim, string _ParamName) {
			foreach (AnimatorControllerParameter param in _Anim.parameters) {
				if (param.name == _ParamName)
					return true;
			}
			return false;
		}

		public static float Truncate(this float value, int digits) {
			double mult = Math.Pow(10.0, digits);
			double result = Math.Truncate(mult * value) / mult;
			return (float) result;
		}

	}
}