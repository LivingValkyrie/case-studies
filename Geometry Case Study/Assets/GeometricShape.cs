﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;

/// <summary>
/// Author: Matt Gipson
/// Contact: Deadwynn@gmail.com
/// Domain: www.livingvalkyrie.net
/// 
/// Description: GeometricShape
/// </summary>
public class GeometricShape : MonoBehaviour {
	#region Fields

	public GameObject vertexPrefab;
	public int sides;
	public float radius = 1;

	int verts;
	float angleBetweenVerts;

	#endregion
	
	void Start() {
		foreach (Transform child in transform) {
			DestroyImmediate(child);
		}

		verts = sides;
		angleBetweenVerts = 360.0f / verts;

		for (int i = 0; i < verts; i++) {
			GameObject vertex = Instantiate( vertexPrefab, transform.position, Quaternion.identity);
			transform.eulerAngles = Vector3.up * angleBetweenVerts * i;
			//print(transform.eulerAngles + " " + transform.forward);
			vertex.transform.position = transform.position + transform.forward * radius;
			vertex.transform.parent = transform;
		}
	}

	void OnValidate() {
		Start();
	}
}